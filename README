GNU Autotools Demo
===================

The code in here serves as an illustration of GNU "autotools" usage.
It builds a program that computes a Fast Fourier Transform (FFT) and
emits a PostScript plot of that FFT.

It is organized as follows:

  - src/fft.c is the main program, which computes the FFT using the GNU
    Scientific Library (GSL).

  - src/view.[ch] is the viewing library based upon libplot, which is
    part of the GNU Plotutils.

This repository contains several branches to illustrate several levels
of build system sophistication:

  - 'master' contains just C code;

  - '0-hand-written-makefile' adds a makefile;

  - '1-autoconf+simple-makefile-template' adds 'configure.ac', and turns
    the makefile into a makefile template;

  - '2-autoconf+automake' replaces the makefile template with a
    'Makefile.am';

  - '3-autoconf+automake+conditional-libplot' makes libplot support
     conditional;

  - '4-autoconf+automake+tests' adds unit tests;

  - '5-autoconf+automake+libtool' uses Libtool to turn plotting support
     into a library.

This code is written as a companion to the talk "GNU Autotools Do What
Your Users Need" given at Inria in Bordeaux in July 2014:

  http://sed.bordeaux.inria.fr/seminars/autotools_20140715.pdf

Ludovic Courtès <ludovic.courtes@inria.fr>.
