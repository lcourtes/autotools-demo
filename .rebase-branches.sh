#!/bin/sh

# This scripts rebases all the branches atop 'master'.

set -e

branches="`git branch -l | cut -c 3- | grep -v '^master$'`"
for branch in $branches
do
    echo "rebasing '$branch'..."
    git checkout "$branch"
    git rebase master
done
