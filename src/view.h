#pragma once

#include <unistd.h>

extern void init_the_thing (void);
extern void plot_the_thing (const double *data, size_t n, const char *color);
extern void spit_the_thing (void);
