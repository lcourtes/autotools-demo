#include <stdio.h>
#include <stdlib.h>
#include <plot.h>

#include "view.h"

static plPlotter *plotter;
static plPlotterParams *plotter_params;

void
init_the_thing ()
{
  /* set Plotter parameters */
  plotter_params = pl_newplparams ();
  pl_setplparam (plotter_params, "PAGESIZE", "letter");

  /* create an X Plotter with the specified parameters */
  plotter = pl_newpl_r ("ps", stdin, stdout, stderr, plotter_params);

  if (pl_openpl_r (plotter) < 0)         /* open Plotter */
    {
      fprintf (stderr, "Couldn't open Plotter\n");
      abort ();
    }
  pl_fspace_r (plotter,
	       -10, -10, 1280, 1280);  /* set user coor system */
  pl_linewidth_r (plotter, 1);           /* set line thickness */
  pl_filltype_r (plotter, 1);            /* objects will be filled */
  pl_bgcolorname_r (plotter, "white"); /* set background color */

  /* draw the first simple path: a large box */
  pl_orientation_r (plotter, 1);
  pl_colorname_r (plotter, "black");
  pl_fline_r (plotter, 0.0, 0.0, 128.0, 0.0);
  pl_fline_r (plotter, 0.0, -128.0, 0.0, 128.0);

  pl_ffontsize_r (plotter, 8.);  /* set font size */
  pl_fmove_r (plotter, -7., -7.);
  pl_label_r (plotter, "0");
  pl_fmove_r (plotter, -7., 80.);
  pl_label_r (plotter, "8");
  pl_fmove_r (plotter, 160., -7.);
  pl_label_r (plotter, "16");

  pl_fmove_r (plotter, 120, 64);
  pl_label_r (plotter, "Mardi du d�v', yay!");
}

void
plot_the_thing (const double *data, size_t n, const char *color)
{
  size_t i = 0;

  pl_linewidth_r (plotter, 0.5);           /* set line thickness */
  for (i = 0; i < n; i++)
    {
      double width = 5.;

      pl_fillcolorname_r (plotter, color);
      pl_pencolorname_r (plotter, "black");
      pl_fbox_r (plotter, i * width, 0.,
  		 (i + 1) * width, data[i] * 10);
    }
}

void
spit_the_thing ()
{
  pl_closepl_r (plotter);
  pl_deletepl_r (plotter);
}
