#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_fft_real.h>

#ifdef HAVE_LIBPLOT
# include <view.h>
#endif

#define NITEMS 64

static void
show_version (void)
{
  printf ("The Incredible GNU Autotools Demo\n");
  printf ("Copyright (C) 2014 Inria\n");
  exit (EXIT_SUCCESS);
}


int
main (int argc, char *argv[])
{
  int i;
  double data[NITEMS];

  if (argc > 1 && strcmp (argv[1], "--version") == 0)
    show_version ();

#ifdef HAVE_LIBPLOT
  init_the_thing ();
#endif

  for (i = 0; i < NITEMS; i++)
    data[i] = 0.;

  for (i = 0; i <= 10; i++)
    data[i] = 1.;

#ifdef HAVE_LIBPLOT
  plot_the_thing (data, NITEMS, "blue");
#else
  for (i = 0; i < NITEMS; i++)
    printf ("%d %e\n", i, data[i]);
  printf ("\n");
#endif

  gsl_fft_real_radix2_transform (data, 1, 64);

#ifdef HAVE_LIBPLOT
  plot_the_thing (data, 64, "light green");
  spit_the_thing ();
#else
  for (i = 0; i < NITEMS; i++)
    printf ("%d %e\n", i, data[i]);
#endif

  return 0;
}
